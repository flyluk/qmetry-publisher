package com.flyluk.jenkins.plugins.qmetry;

import hudson.Extension;
import hudson.FilePath;
import hudson.Launcher;
import hudson.model.AbstractProject;
import hudson.model.Result;
import hudson.model.Run;
import hudson.model.TaskListener;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.BuildStepMonitor;
import hudson.tasks.Publisher;
import jenkins.tasks.SimpleBuildStep;
import org.jenkinsci.Symbol;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.kohsuke.stapler.DataBoundConstructor;

import java.io.*;
import java.util.Iterator;

public class QMetryPublisher extends Publisher implements SimpleBuildStep {

    @DataBoundConstructor
    public QMetryPublisher() {
    }

    @Override
    public void perform(Run<?, ?> run, FilePath workspace, Launcher launcher, TaskListener listener) throws InterruptedException, IOException {
        PrintStream logger = listener.getLogger();
        logger.println("Collecting Qmetry Report");
        InputStreamReader reader = null;
        String buildReportDir = null;
        try {
            String path = workspace.toString() + "/test-results/meta-info.json";
            JSONObject report = readJSONfromFile(path);
            if ( report != null ) {
                JSONArray reportList = (JSONArray) report.get("reports");
                Iterator<JSONObject> iterator = reportList.iterator();

                long startTime = run.getStartTimeInMillis();
                //logger.println(startTime);

                while (iterator.hasNext()) {
                    JSONObject reportDetails = iterator.next();
                    long reportTime = (Long) reportDetails.get("startTime");
                    String dir = (String) reportDetails.get("dir");

                    if (reportTime >= startTime) {
                        buildReportDir = dir;
                        logger.println(buildReportDir + " will be used for the build");
                    }
                }
            } else {
                logger.println("Qmetry test results cannot be found! Publishing Skipped");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if ( reader != null )
                reader.close();
        }

        try {
            if ( buildReportDir != null ) {
                String path = workspace.toString() + "/" + buildReportDir + "/meta-info.json";
                String status = "PASSED";
                JSONObject testReult = readJSONfromFile(path);
                long total = (Long) testReult.get("total");
                long pass = (Long) testReult.get("pass");
                if (pass != total) {
                    status = "FAILED";
                    run.setResult(Result.UNSTABLE);
                }
                logger.println(pass + "/" + total + " : [" + status + "]");
            }
        } catch ( Exception e ) {
            e.printStackTrace();
        }

    }

    @Override
    public BuildStepMonitor getRequiredMonitorService() {
        return BuildStepMonitor.NONE;
    }

    @Symbol("QmetryPublisher")
    @Extension
    public static final class DescriptorImpl extends BuildStepDescriptor<Publisher> {

        @Override
        public boolean isApplicable(Class<? extends AbstractProject> aClass) {
            return true;
        }

        @Override
        public String getDisplayName() {
            return Messages.QMetryPublisher_DescriptorImpl_DisplayName();
        }

    }

    private JSONObject readJSONfromFile(String path)
    {
        JSONObject jsonObj = null;
        try {

            InputStreamReader reader = new InputStreamReader(new FileInputStream(path), "UTF-8");
            JSONParser parser = new JSONParser();
            jsonObj = (JSONObject) parser.parse(reader);

        } catch ( FileNotFoundException e ) {
            //e.printStackTrace();
            //e.getMessage();

        } catch ( ParseException e ) {
            //e.printStackTrace();
        } catch ( UnsupportedEncodingException e ){
            //e.printStackTrace();
        } catch ( IOException e ) {
            //e.printStackTrace();
        }
        return jsonObj;

    }

}
